"""
關於第十四章投影片用的範例
"""


def ex14_1():
    import os
    print(os.getcwd())
    print(os.path.abspath('.'))
    print(os.path.abspath('..'))
    print(os.path.abspath('ch14.py'))
    print(os.path.relpath('C:\\'))
    #\跟\\效果一樣
    print(os.path.relpath('C:\\Users\\Spock_Xie\\Desktop\\python\\'))
    
    
def ex14_2():
    import os
    print(os.getcwd())
    print("{:*^20} ".format("EXIST"))    
    print(os.path.exists('PPT\ch14.py'))
    #不知道為啥只有第一個要\\
    print(os.path.exists("C:\\Users\Spock_Xie\Desktop\python"))
    print("{:*^20} ".format("ABS"))    
    print(os.path.isabs('PPT\ch14.py'))
    print(os.path.isabs("C:\\Users\Spock_Xie\Desktop\python"))
    print("{:*^20} ".format("ISDIR"))    
    print(os.path.isdir('PPT\ch14.py'))
    print(os.path.isdir("C:\\Users\Spock_Xie\Desktop\python"))
    print("{:*^20} ".format("ISFILE"))    
    print(os.path.isfile('PPT\ch14.py'))
    print(os.path.isfile("C:\\Users\Spock_Xie\Desktop\python"))


def ex14_3():
    import os
    os.mkdir("file_test") if not os.path.exists("file_test") else True
    with open("file_test\\test.txt",'w') as f:
        f.write("test")
    os.remove("file_test\\test.txt")
    os.rmdir("file_test")

    print(os.getcwd())
    os.chdir("..")
    print(os.getcwd())

   

def ex14_4():
    import os
    print(os.path.join("C:\\"))
    print(os.path.join("C:\\","Users"))
    print(os.path.join("C:\\","Users","Spock_Xie"))
    print(os.path.getsize('PPT\ch14.py'))
    print(os.listdir('PPT'))
    
     

def ex14_5():
    import glob
    for file in glob.glob("C:\\Users\Spock_Xie\Desktop\python\*.*"):
        print(file)
    print("{:*^20} ".format("")) 
    for file in glob.glob("PPT\*.py"):
        print(file)
    print("{:*^20} ".format("")) 
    for file in glob.glob("PPT\ch1*.py"):
        print(file)
    


    

def ex14_6():
    import os
    for dirname,subdir,file in os.walk("PPT"):
        print("dir:",dirname)
        print("subdir:",subdir)
        print("subdir:",file)
        print("{:*^20} ".format("")) 
    

def ex14_7():
    fn = 'PPT\\Temp\\read.txt'
    file = open(fn)
    data = file.read()
    print(data)
    print("{:*^20} ".format("")) 
    

    with  open(fn) as file:     
        for line in file:
            print(line,end='')

    with  open(fn) as file:
        #file像是疊代器讀完後就沒了
        data = file.read()
        print(data.replace("was","***"))
        print("{:*^20} ".format(""))
        #find 第一次出現 rfind 最後一次出現
        print(data.find("and"))
        print(data.rfind("and"))
        print("{:*^20} ".format(""))    
    with  open(fn) as file:
        #file像是疊代器讀完後就沒了
        msg=''
        while True:
            data = file.read(100)
            msg=msg+data
            if not data:
                break
        print(msg)

def ex14_8():
    fn = 'PPT\\Temp\\write.txt'
    string = "ppppppppppthon"
    with open(fn, 'w') as f:
        #會輸出寫入的長度
        print(len(string))
        print(f.write(string))
        print(f.write(str(1000)+'\n'))
    with open(fn, 'a') as f:
        print(f.write("appppppppppppeeeeeeend"))
    long_string="""
                He sat up front on her wedding day
                And cried as his daughter walked away.
                Later that night he watched her dance.
                He sat there waiting for his chance.
                The band started to play their song.
                Father and daughter danced along.
                """
    
    with open(fn, 'a') as f:
        length = len(long_string)
        print("length:",length)
        offset = 0
        chunk = 30
        while offset < length :
            #offset+chunk>length也可以他會自動處理
            f.write(long_string[offset:offset+chunk])
            offset+=chunk

def ex14_9():
    file='PPT\\Temp\\wrb'
    bytedata=bytes(range(0,256))
    with open(file,'wb') as f:
        f.write(bytedata)

    with open(file,'rb') as f:
        temp=f.read()
        print(temp[0])
        #seek可以移動讀取位置
        f.seek(10)
        temp=f.read()
        print(temp[0])
        f.seek(255)
        temp=f.read()
        print(temp[0])

def ex14_10():
    import  shutil
    shutil.copy('PPT\\Temp\\write.txt','PPT\\Temp\\writecopy.txt')
    try:
        shutil.copytree('PPT\\Temp','PPT\\Temp2')
    except:
        pass
    shutil.move('PPT\\Temp\\writecopy.txt','PPT\\Temp\\move\\123.txt')
    shutil.rmtree('PPT\\Temp2' )

def ex14_11():
    file='PPT\\Temp\\to_be_delete'  
    with open(file,'w') as f:
        f.write("test")
    import	send2trash	
    send2trash.send2trash('PPT\\Temp\\to_be_delete')

def ex14_12():
    import  zipFile    
    file='PPT\\Temp\\to_be_zip'  
    with open(file,'w') as f:
        f.write("test")
    zipfile.ZipFile("test.zip",'w')

def ex14_13():
    file='PPT\\Temp\\encode.txt'
    file_utf='PPT\\Temp\\encode_utf8.txt'
    data=''
    with open(file,'r',encoding="cp950") as f:
        data=f.read()
        print(data)
    
    with open(file_utf,'r',encoding="utf-8") as f:
        data=f.read()
        print(data)
    with open(file_utf,'r',encoding="utf-8") as f:
        data=f.readlines()
        print(data)
    with open(file_utf,'r',encoding="utf-8-sig") as f:
        data=f.readlines()
        print(data)

def ex14_14():
    import pyperclip
    #pyperclip.copy("copy testing")
    print(pyperclip.paste())



if __name__=='__main__':
    ex14_14()
    